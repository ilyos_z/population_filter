package com.example.population_filter.payload;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class ReqUserAndHomes {
    private UUID userId;
    private List<UUID> newHomes;

}
