package com.example.population_filter.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class ReqHome {
    private Integer homeNum;
    private UUID quarterId;
}
