package com.example.population_filter.payload;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class ReqUser {
    private String userName;
    private String password;
    private List<UUID> homeIds;
    private List<Integer> userRoles;
}
