package com.example.population_filter.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class ReqOthers {
    private String name;
    private UUID parentId;
}
