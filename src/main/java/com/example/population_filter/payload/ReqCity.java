package com.example.population_filter.payload;

import lombok.Data;

@Data
public class ReqCity {
    private String name;
}
