package com.example.population_filter.payload;

import lombok.Data;

@Data
public class ReqSignIn {

    private String userName;
    private String password;
}
