package com.example.population_filter.controller;

import com.example.population_filter.payload.ReqOthers;
import com.example.population_filter.service.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/district")
public class DistrictController {

    @Autowired
    DistrictService districtService;

    @PostMapping
    @Secured({"DISTRICT_USER","CITY_USER"})
    public HttpEntity<?> saveDistrict(@RequestBody ReqOthers reqDistrict){
        return ResponseEntity.ok(districtService.saveDistrict(reqDistrict));
    }


    @GetMapping
    public HttpEntity<?> getDistrict(@RequestParam UUID cityId){
        return ResponseEntity.ok(districtService.getDistrictByCity(cityId));
    }
}
