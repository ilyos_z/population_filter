package com.example.population_filter.controller;


import com.example.population_filter.payload.ReqCity;
import com.example.population_filter.service.CityService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/city")
public class CityController {
    @Autowired
    CityService cityService;

    @PostMapping
    @Secured("CITY_USER")
    public HttpEntity<?> saveCity(@RequestBody ReqCity reqCity){
        return ResponseEntity.ok(cityService.saveCity(reqCity));
    }


    @GetMapping
    public HttpEntity<?> getCities(){
        return ResponseEntity.ok(cityService.getCities());
    }

}
