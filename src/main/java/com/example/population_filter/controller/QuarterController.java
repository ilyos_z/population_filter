package com.example.population_filter.controller;

import com.example.population_filter.payload.ReqOthers;
import com.example.population_filter.service.DistrictService;
import com.example.population_filter.service.QuarterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/quarter")
public class QuarterController {

    @Autowired
    QuarterService quarterService;

    @PostMapping
    @Secured({"QUARTER_USER","CITY_USER"})
    public HttpEntity<?> saveQuarter(@RequestBody ReqOthers reqQuarter){
        return ResponseEntity.ok(quarterService.saveQuarter(reqQuarter));
    }


    @GetMapping
    public HttpEntity<?> getQuarter(@RequestParam UUID districtId){
        return ResponseEntity.ok(quarterService.getQuartersByDistrict(districtId));
    }
}
