package com.example.population_filter.controller;

import com.example.population_filter.payload.ReqHome;
import com.example.population_filter.payload.ReqOthers;
import com.example.population_filter.service.DistrictService;
import com.example.population_filter.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/home")
public class HomeController {

    @Autowired
    HomeService homeService;

    @PostMapping
    @Secured({"USER"})
    public HttpEntity<?> saveHome(@RequestBody ReqHome reqHome){
        return ResponseEntity.ok(homeService.saveHome(reqHome));
    }


    @GetMapping
    public HttpEntity<?> getHome(@RequestParam UUID quarterId){
        return ResponseEntity.ok(homeService.getHomesByQuarter(quarterId));
    }
}
