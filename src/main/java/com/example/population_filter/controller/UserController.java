package com.example.population_filter.controller;

import com.example.population_filter.payload.*;
import com.example.population_filter.repository.RoleRepository;
import com.example.population_filter.security.AuthService;
import com.example.population_filter.security.JwtTokenProvider;
import com.example.population_filter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/auth")
public class UserController {
    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    AuthenticationManager authenticate;

    @Autowired
    AuthService authService;

    @Autowired
    UserService userService;

    @Autowired
    RoleRepository roleRepository;

    @GetMapping("/role")
    public HttpEntity<?> getAllRolesToSelect(){
        return ResponseEntity.ok(new ApiResponse("success",true, roleRepository.findAll()));
    }

    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody ReqSignIn reqSignIn){
        return ResponseEntity.ok(getApiToken(reqSignIn.getUserName(),reqSignIn.getPassword()));
    }

    @PostMapping("/register")
    public HttpEntity<?> register(@RequestBody ReqUser reqUser){
        return ResponseEntity.ok(userService.register(reqUser));
    }

    @PostMapping("/home")
    public HttpEntity<?> editHomeOfUser(@RequestBody ReqUserAndHomes reqUserAndHomes){
        return ResponseEntity.ok(userService.editUserHomes(reqUserAndHomes));
    }

    @GetMapping("/home")
    public HttpEntity<?> getAllUsersOfHome(@RequestParam UUID homeId){
        return ResponseEntity.ok(userService.getAllUsersOfHome(homeId));
    }


    @GetMapping
    public HttpEntity<?> getAllHomesOfUser(@RequestParam UUID userId){
        return ResponseEntity.ok(userService.getAllHomesOfUser(userId));
    }

    public HttpEntity<?> getApiToken(String phoneNumberOrEmail, String password){
        Authentication authentication = authenticate.authenticate(
                new UsernamePasswordAuthenticationToken(phoneNumberOrEmail, password)
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtResponse(jwt));
    }






}
