package com.example.population_filter.service;

import com.example.population_filter.entity.City;
import com.example.population_filter.entity.District;
import com.example.population_filter.payload.ApiResponse;
import com.example.population_filter.payload.ReqOthers;
import com.example.population_filter.repository.CityRepository;
import com.example.population_filter.repository.DistrictRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class DistrictService {
    @Autowired
    CityRepository cityRepository;

    @Autowired
    DistrictRepository districtRepository;

    public ApiResponse saveDistrict(ReqOthers reqDistrict){
        City city = cityRepository.findById(reqDistrict.getParentId()).get();
        districtRepository.save(new District(reqDistrict.getName(),city));
        return new ApiResponse("success",true);
    }

    public ApiResponse getDistrictByCity(UUID cityId){
        return new ApiResponse("success",true,districtRepository.getDistrictsByCity(cityId));
    }
}
