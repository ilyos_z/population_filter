package com.example.population_filter.service;

import com.example.population_filter.entity.City;
import com.example.population_filter.payload.ApiResponse;
import com.example.population_filter.payload.ReqCity;
import com.example.population_filter.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityService {

    @Autowired
    CityRepository cityRepository;

    public ApiResponse saveCity(ReqCity reqCity){
        cityRepository.save(new City(reqCity.getName()));
        return new ApiResponse("Success",true);
    }

    public ApiResponse getCities(){
        return new ApiResponse("success",true,cityRepository.findAll());
    }



}
