package com.example.population_filter.service;

import com.example.population_filter.entity.Home;
import com.example.population_filter.entity.Role;
import com.example.population_filter.entity.User;
import com.example.population_filter.payload.ApiResponse;
import com.example.population_filter.payload.ReqSignIn;
import com.example.population_filter.payload.ReqUser;
import com.example.population_filter.payload.ReqUserAndHomes;
import com.example.population_filter.repository.HomeRepository;
import com.example.population_filter.repository.RoleRepository;
import com.example.population_filter.repository.UserRepository;
import com.example.population_filter.security.AuthService;
import com.example.population_filter.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;


    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    AuthenticationManager authenticate;

    @Autowired
    HomeRepository homeRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AuthService authService;

    public ApiResponse register(ReqUser reqUser){
        List<Home> userHomes = homeRepository.findAllById(reqUser.getHomeIds());
        List<Role> userRoles = roleRepository.findAllById(reqUser.getUserRoles());
        Set<Role> userRolesAsSet = new HashSet<Role>(userRoles);
        User savedUser = userRepository.save(new User(
                reqUser.getUserName(),
                passwordEncoder.encode(reqUser.getPassword()),
                userRolesAsSet,
                userHomes
        ));
        Authentication authentication = authenticate.authenticate(
                new UsernamePasswordAuthenticationToken(savedUser.getUsername(), reqUser.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtTokenProvider.generateToken(authentication);
        return new ApiResponse("success",true,jwt);
    }


    public ApiResponse editUserHomes(ReqUserAndHomes reqUserAndHomes){
        User user = userRepository.findById(reqUserAndHomes.getUserId()).get();
        List<Home> newHomes = homeRepository.findAllById(reqUserAndHomes.getNewHomes());
        user.setHomes(newHomes);
        User save = userRepository.save(user);
        return new ApiResponse("Success",true,save);
    }

    public ApiResponse getAllUsersOfHome(UUID homeId){
        return new ApiResponse("Success",true,userRepository.getUsersByHomeId(homeId));
    }


    public ApiResponse getAllHomesOfUser(UUID userId){
        return new ApiResponse("Success",true,userRepository.getHomesOfUser(userId));
    }
}
