package com.example.population_filter.service;

import com.example.population_filter.entity.District;
import com.example.population_filter.entity.Quarter;
import com.example.population_filter.payload.ApiResponse;
import com.example.population_filter.payload.ReqOthers;
import com.example.population_filter.repository.DistrictRepository;
import com.example.population_filter.repository.QuarterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class QuarterService {
    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    QuarterRepository quarterRepository;

    public ApiResponse saveQuarter(ReqOthers reqOthers){
        District district = districtRepository.findById(reqOthers.getParentId()).get();
        quarterRepository.save(new Quarter(
                reqOthers.getName(),district
        ));

        return new ApiResponse("success",true);
    }

    public ApiResponse getQuartersByDistrict(UUID districtId){
        return new ApiResponse("success",true,quarterRepository.getQuartersByDistrictId1(districtId));
    }

}
