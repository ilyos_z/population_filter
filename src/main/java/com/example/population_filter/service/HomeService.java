package com.example.population_filter.service;

import com.example.population_filter.entity.Home;
import com.example.population_filter.entity.Quarter;
import com.example.population_filter.payload.ApiResponse;
import com.example.population_filter.payload.ReqHome;
import com.example.population_filter.payload.ReqOthers;
import com.example.population_filter.repository.HomeRepository;
import com.example.population_filter.repository.QuarterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class HomeService {
    @Autowired
    QuarterRepository quarterRepository;

    @Autowired
    HomeRepository homeRepository;

    public ApiResponse saveHome(ReqHome reqHome){
        Quarter quarter = quarterRepository.findById(reqHome.getQuarterId()).get();
        homeRepository.save(new Home(reqHome.getHomeNum(),quarter));
        return new ApiResponse("sucess",true);
    }


    public ApiResponse getHomesByQuarter(UUID quarterId){
        List<Home> allByQuarterId = homeRepository.findAllByQuarterId(quarterId);
        return new ApiResponse("success",true,allByQuarterId);
    }
}

