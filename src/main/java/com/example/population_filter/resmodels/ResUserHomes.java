package com.example.population_filter.resmodels;

import java.util.UUID;

public interface ResUserHomes {
    UUID getUserId();
    String getUserName();
    Integer getHomeNumber();
    String getQuarterName();
    String getDistrictName();
    String getCityName();
}
