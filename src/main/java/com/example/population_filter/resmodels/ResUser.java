package com.example.population_filter.resmodels;

import java.util.UUID;

public interface ResUser {
    UUID getId();
    String getUserName();
}
