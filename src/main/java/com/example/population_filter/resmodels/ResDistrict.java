package com.example.population_filter.resmodels;

import java.util.UUID;

public interface ResDistrict {
    UUID getId();
    String getDistrictName();
}
