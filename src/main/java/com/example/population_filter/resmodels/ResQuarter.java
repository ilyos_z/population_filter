package com.example.population_filter.resmodels;

import java.util.UUID;

public interface ResQuarter {
    UUID getId();
    String getQuarterName();
}
