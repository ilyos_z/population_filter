package com.example.population_filter.component;

import com.example.population_filter.entity.*;
import com.example.population_filter.entity.enums.RoleName;
import com.example.population_filter.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;


    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    CityRepository cityRepository;

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    HomeRepository homeRepository;

    @Autowired
    QuarterRepository quarterRepository;

    @Autowired
    PasswordEncoder passwordEncoder;


    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            List<Role> roles = new ArrayList<>();
            roles.add(new Role(1, RoleName.USER));
            roles.add(new Role(2, RoleName.CITY_USER));
            roles.add(new Role(3,RoleName.DISTRICT_USER));
            roles.add(new Role(4,RoleName.QUARTER_USER));
            List<Role> userRoles = roleRepository.saveAll(roles);

            List<City> cities = new ArrayList<>();
            cities.add(new City("Tashkent"));
            cities.add(new City("Samarkand"));

            List<City> savedCities = cityRepository.saveAll(cities);

            List<District> districtsTashkent = new ArrayList<>();
            List<District> districtsSamarkand = new ArrayList<>();



            districtsTashkent.add(new District("Shayxontoxur", savedCities.get(0)));
            districtsTashkent.add(new District("Chilonzor", savedCities.get(0)));

            districtsSamarkand.add(new District("Jomboy", savedCities.get(1)));
            districtsSamarkand.add(new District("Narpay", savedCities.get(1)));


            List<District> savedDistrictsTashkent = districtRepository.saveAll(districtsTashkent);
            List<District> savedDistrictsSamarkand = districtRepository.saveAll(districtsSamarkand);


            List<Quarter> quartersShayxontoxur = new ArrayList<>();
            List<Quarter> quartersChilonzor = new ArrayList<>();

            List<Quarter> quartersJomboy = new ArrayList<>();
            List<Quarter> quartersNarpay = new ArrayList<>();


            quartersShayxontoxur.add(new Quarter(
                    "Shofayzi",savedDistrictsTashkent.get(0)
            ));
            quartersShayxontoxur.add(new Quarter(
                    "Labzak",savedDistrictsTashkent.get(0)
            ));

            quartersChilonzor.add(new Quarter(
                    "Qatortol",savedDistrictsTashkent.get(1)
            ));
            quartersChilonzor.add(new Quarter(
                    "Qovunchi",savedDistrictsTashkent.get(1)
            ));

            quartersJomboy.add(new Quarter(
                    "Guliston",savedDistrictsSamarkand.get(0)
            ));
            quartersJomboy.add(new Quarter(
                    "Zarafshon",savedDistrictsSamarkand.get(0)
            ));

            quartersNarpay.add(new Quarter(
                    "test narpay",savedDistrictsSamarkand.get(1)
            ));
            quartersNarpay.add(new Quarter(
                    "test narpay",savedDistrictsSamarkand.get(1)
            ));


            quarterRepository.saveAll(quartersChilonzor);
            List<Quarter> quarters = quarterRepository.saveAll(quartersShayxontoxur);
            quarterRepository.saveAll(quartersJomboy);
            quarterRepository.saveAll(quartersNarpay);

            Home savedHome = homeRepository.save(new Home(13, quarters.get(0)));

            List<Home> homes = new ArrayList<>();
            homes.add(savedHome);

            Set<Role> userRoleSet = new HashSet<Role>(userRoles);

            userRepository.save(new User("Ilyos", passwordEncoder.encode("ilyos"),userRoleSet,homes));


        }
    }
}
