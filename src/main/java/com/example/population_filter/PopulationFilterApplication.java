package com.example.population_filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PopulationFilterApplication {

    public static void main(String[] args) {
        SpringApplication.run(PopulationFilterApplication.class, args);
    }

}
