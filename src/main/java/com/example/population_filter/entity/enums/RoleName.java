package com.example.population_filter.entity.enums;

public enum RoleName {
    USER,
    CITY_USER,
    DISTRICT_USER,
    QUARTER_USER
}
