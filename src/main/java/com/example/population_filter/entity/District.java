package com.example.population_filter.entity;


import com.example.population_filter.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class District extends AbsEntity {
    private String districtName;

    @ManyToOne
    private City city;
}
