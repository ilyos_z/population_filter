package com.example.population_filter.entity;

import com.example.population_filter.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class City extends AbsEntity {
        private String cityName;
}
