package com.example.population_filter.repository;

import com.example.population_filter.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    @Query(value = "select * from role where name=:role",nativeQuery = true)
    Role findByNameIn(@Param(value = "role") String role);
}
