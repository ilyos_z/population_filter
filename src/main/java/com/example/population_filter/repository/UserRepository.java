package com.example.population_filter.repository;

import com.example.population_filter.entity.User;
import com.example.population_filter.resmodels.ResUser;
import com.example.population_filter.resmodels.ResUserHomes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByUserName(String userName);

    boolean existsByUserName(String phoneNumber);

    @Query(value = "select users.id , users.user_name from users join users_homes uh on users.id = uh.users_id join home h on h.id = uh.homes_id where h.id=:homeId",nativeQuery = true)
    List<ResUser> getUsersByHomeId(@Param(value = "homeId")UUID homeId);

    @Query(value = "select users.id as userId, users.user_name as userName, home_number as homeNumber, quarter_name as quarterName, district_name as districtName, city_name as cityName from users join users_homes uh on users.id = uh.users_id join home h on h.id = uh.homes_id join quarter q on h.quarter_id = q.id join district d on q.district_id = d.id join city c on d.city_id = c.id where users_id=:userId",nativeQuery = true)
    List<ResUserHomes> getHomesOfUser(@Param(value = "userId") UUID userId);
}
