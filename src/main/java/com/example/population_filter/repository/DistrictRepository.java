package com.example.population_filter.repository;

import com.example.population_filter.entity.District;
import com.example.population_filter.resmodels.ResDistrict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface DistrictRepository extends JpaRepository<District, UUID> {
    @Query(value = "select cast(district.id as varchar) as id,  district_name as districtName from district join city c on c.id = district.city_id where c.id=:cityId",nativeQuery = true)
    List<ResDistrict> getDistrictsByCity(@Param(value = "cityId")UUID cityId);
}
