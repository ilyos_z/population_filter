package com.example.population_filter.repository;

import com.example.population_filter.entity.Quarter;
import com.example.population_filter.resmodels.ResQuarter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface QuarterRepository extends JpaRepository<Quarter, UUID> {

    @Query(value = "select cast(q.id as varchar) as id, q.quarter_name as quarterName from quarter q where q.district_id=:districtId ",nativeQuery = true)
    List<ResQuarter> getQuartersByDistrictId1(@Param(value = "districtId")UUID id);

}
