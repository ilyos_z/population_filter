package com.example.population_filter.repository;

import com.example.population_filter.entity.City;

import org.springframework.data.jpa.repository.JpaRepository;


import java.util.UUID;

public interface CityRepository extends JpaRepository<City, UUID> {

}
