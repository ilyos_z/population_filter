package com.example.population_filter.repository;

import com.example.population_filter.entity.Home;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface HomeRepository extends JpaRepository<Home, UUID> {
    List<Home> findAllByQuarterId(UUID id);
}
